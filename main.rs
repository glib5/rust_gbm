// #![allow(unused)]

use rand::{distributions::StandardNormal, SeedableRng, prelude::SmallRng, Rng};

#[derive(Debug)]
struct GBMParams{
    // minimum info required to simulate a GBM
    float_reps:f64,
    s0:f64,
    g_scale:f64,
    mu_scale:f64,
    steps:usize,
    reps:usize,
}

fn get_params() -> GBMParams {
    // gets params from somewhere
    let steps: usize = 12;
    let reps: usize = 1<<21;
    let mu = 0.0;
    let sd = 0.3;
    let dt = 1.0/(steps as f64);
    let s0 = 4.0;
    // also, store here constants that are needed in the computations
    let float_reps = {reps as f64};
    let g_scale = sd*(dt.sqrt());
    let mu_scale = (mu - 0.5*sd*sd)*dt;
    // nicely pack and return them
    return GBMParams{
        float_reps,
        s0,
        g_scale,
        mu_scale,
        steps,
        reps,
    };
}

fn make_traj(p: &GBMParams, arr: &mut Vec<f64>, rng: &mut SmallRng){
    // generates a GBM trajectory
    for i in 1..(p.steps+1){
        let g: f64 = rng.sample(StandardNormal);
        arr[i] = arr[i-1] + p.mu_scale + g*p.g_scale;
        arr[i-1] = p.s0*arr[i-1].exp();
    }
    arr[p.steps] = p.s0*arr[p.steps].exp();
}

fn mc(p: &GBMParams, fm: &mut Vec<f64>, sm: &mut Vec<f64>){
    // monte carlo simulation
    println!("{:20}", p.reps);
    // set rng
    let mut rng = SmallRng::seed_from_u64(4312 as u64);
    // MC routine
    for i in 0..p.reps{
        let mut arr = vec![0.0; p.steps+1];
        make_traj(&p, &mut arr, &mut rng);
        // store for stats
        for j in 1..(p.steps+1){
            fm[j] += arr[j];
            sm[j] += arr[j]*arr[j];
        }
        // show count
        if i % (1<<12) == 0 {
            print!("{:20}\r", i);
        }
    }
    println!("");
    // mc stats
    for i in 1..(p.steps+1){
        fm[i] /= p.float_reps;
        sm[i] = 3.0*((sm[i]/(p.float_reps) - fm[i]*fm[i])/(p.float_reps)).sqrt();
    }   
}

// ----------------------------------------------------------------------------

fn main(){
    // params
    let params: GBMParams = get_params();   
    // computations
    let mut fm = vec![0.0; params.steps+1];
    fm[0] = params.s0;
    let mut sm = vec![0.0; params.steps+1];
    mc(&params, &mut fm, &mut sm);
    // results
    for i in 0..fm.len(){
        println!("{}", format!("{}\t{:.4}\t\t{:.4}", i, fm[i], sm[i]));
    }
}